# Spike: TreeViews

|Apartado/Framework|PrimeReact|Ionic|Clarity Core| Blueprint |SemanticUI| FluentUI| ReactSuite |
|---|---|---|---|---|---| --- |---|
| Licencia| Open Source| Open Source | Open Source| Open Source |Open Source| Open Source | Open Source |
| TreeView | Completo | No |No| Básico | Simulado con listas |No|Completo|
| DnD | Sí | No  |No| No | No | Sí, listas| Sí  |
| Limitaciones| hijos se convierten en padres | NA |NA| Sólo con typescript | Basado en JQuery y listas| Solo reordering |requiere --legacy-peer-deps para instalar y los hijos se convierten en padres|