import "./App.css";
import AntTreeComponent from "./antd/AntTreeComponent";

function App() {
  return (
    <div className="App">
      <h1 style={{ textAlign: "center" }}>
        <i>Ant</i>
      </h1>
      <hr />
      <div style={{ marginLeft: "20%", marginRight: "20%" }}>
        <AntTreeComponent />
      </div>

      {/* <h1 style={{ textAlign: "center" }}>
        <i>RCTreeDraggable</i>
      </h1>
      <hr />
      <div style={{ marginLeft: "20%", marginRight: "20%" }}>
        <RCTreeDraggable />
      </div> */}
    </div>
  );
}

export default App;
