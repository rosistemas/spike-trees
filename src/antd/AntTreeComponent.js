import React, { useState } from "react";
import { Tree, Menu, Dropdown } from "antd";
import "antd/dist/antd.css";
import {
  TextField,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText,
  Grid,
} from "@material-ui/core";

//#region Generador de datos
const x = 3;
const y = 1;
const z = 1;
const gData = [];

const generateFakeData = (_level, _preKey, _tns) => {
  const preKey = _preKey || "0";
  const tns = _tns || gData;

  const children = [];
  for (let i = 0; i < x; i++) {
    const key = `${preKey}-${i}`;
    tns.push({ title: key, key });
    if (i < y) {
      children.push(key);
    }
  }
  if (_level < 0) {
    return tns;
  }
  const level = _level - 1;
  children.forEach((key, index) => {
    tns[index].children = [];
    return generateFakeData(level, key, tns[index].children);
  });
};
generateFakeData(z);
//#endregion

const AntTreeComponent = () => {
  const [treeState, setTreeState] = useState({
    gData,
    //Las keys son independientes, no representan estructura.
    expandedKeys: ["0-0", "carpeta"],
    menu: (
      <Menu>
        <Menu.Item key="1">Ejemplo</Menu.Item>
      </Menu>
    ),
  });

  const [nameState, setNameState] = useState({
    value: "",
  });
  const [newNameState, setNewNameState] = useState("");

  const [dialogOpenState, setDialogOpenState] = useState({
    open: false,
    node: "",
  });

  //#region DnD events
  const allowDrop = (info) => {
    const dropNode = info.dropNode;
    const dropPosition = info.dropPosition;

    return dropNode.parent || dropPosition !== 0; // Permitir sólo al mismo nivel o superior.
  };

  const onDrop = (info) => {
    const dropKey = info.node.key;
    const dragKey = info.dragNode.key;
    const dropPos = info.node.pos.split("-");
    const dropPosition =
      info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const data = [...treeState.gData];

    // Find dragObject
    let dragObj;
    fetchNode(data, dragKey, (item, index, arr) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    if (!info.dropToGap) {
      fetchNode(data, dropKey, (item) => {
        item.children = item.children || [];
        item.children.unshift(dragObj);
      });
    } else if (
      (info.node.props.children || []).length > 0 && // Has children
      info.node.props.expanded && // Is expanded
      dropPosition === 1 // On the bottom gap
    ) {
      fetchNode(data, dropKey, (item) => {
        item.children = item.children || [];
        item.children.unshift(dragObj);
      });
    } else {
      let ar;
      let i;
      fetchNode(data, dropKey, (item, index, arr) => {
        ar = arr;
        i = index;
      });
      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj);
      } else {
        ar.splice(i + 1, 0, dragObj);
      }
    }

    setTreeState((oldState) => {
      return {
        ...oldState,
        gData: data,
      };
    });
  };
  //#endregion

  //#region Custom handlers

  const handleNameInputChange = (event) => {
    setNameState({ value: event.target.value });
  };

  const handleRenameInputChange = (event) => {
    setNewNameState(event.target.value);
  };

  const handleNodeInsert = () => {
    // Obtener el state del form
    setTreeState((oldState) => {
      // Lo duplico para evitar escribir sobre el estado actual
      const newTree = [...oldState.gData];
      //Insertar un nodo HOJA al final
      newTree.push({
        title: nameState.value,
        key: "0-" + nameState.value,
        parent: true, // Indicador para la función Allow Drop
      });
      //
      return {
        ...oldState,
        gData: newTree,
      };
    });
  };

  const handleRightClick = (event) => {
    setTreeState((oldState) => {
      return {
        ...oldState,
        menu: (
          <Menu>
            <Menu.Item
              key="1"
              onClick={() => {
                openRenameDialog(event.node);
              }}
            >
              Rename {event.node.title}
            </Menu.Item>
            <Menu.Item
              key="2"
              onClick={() => {
                handleRemoval(event.node);
              }}
            >
              Delete {event.node.title}
            </Menu.Item>
            <Menu.Item
              key="3"
              onClick={() => {
                handleInsertOnLocation(event.node);
              }}
            >
              Insert here
            </Menu.Item>
          </Menu>
        ),
      };
    });
  };

  const openRenameDialog = (node) => {
    setDialogOpenState({
      node: node,
      open: true,
    });
  };

  const handleClose = () => {
    setDialogOpenState((oldState) => {
      return {
        ...oldState,
        open: false,
      };
    });
  };

  // Busca al nodo seleccionado, modifica una referencia para mantener la estructura y actualiza el estado con el nuevo árbol.
  const handleRenameOnClose = () => {
    const updatedNode = { ...dialogOpenState.node };
    updatedNode.title = newNameState;
    const nodeToUpdateKey = { ...updatedNode };
    const duplicatedTree = [...treeState.gData];
    let originalNodeClone;

    fetchNode(duplicatedTree, nodeToUpdateKey.key, (item, index, arr) => {
      originalNodeClone = item;
    });

    originalNodeClone.title = updatedNode.title;
    setTreeState((oldState) => {
      return {
        ...oldState,
        gData: duplicatedTree,
      };
    });

    setDialogOpenState((oldState) => {
      return {
        ...oldState,
        open: false,
      };
    });
  };

  // Busca al nodo seleccionado, lo elimina del array y actualiza el estado con el nuevo árbol.
  const handleRemoval = (node) => {
    const keyToRemove = node.key;
    const duplicatedTree = [...treeState.gData];

    fetchNode(duplicatedTree, keyToRemove, (item, index, arr) => {
      arr.splice(index, 1);
    });

    setTreeState((oldState) => {
      return {
        ...oldState,
        gData: duplicatedTree,
      };
    });
  };

  const handleInsertOnLocation = (node) => {
    const keyToRemove = node.key;
    const duplicatedTree = [...treeState.gData];

    fetchNode(duplicatedTree, keyToRemove, (item, index, arr) => {
      arr.splice(index, 0, { title: 'value', key: `0-${Math.floor(Math.random() * 10)}-${Math.floor(Math.random() * 10)}`, parent: true });

    });

    setTreeState((oldState) => {
      return {
        ...oldState,
        gData: duplicatedTree,
      };
    });
  };


  const fetchNode = (data, key, callback) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].key === key) {
        return callback(data[i], i, data);
      }
      if (data[i].children) {
        fetchNode(data[i].children, key, callback);
      }
    }
  };

  //#endregion

  return (
    <>
      <Dialog
        open={dialogOpenState.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Rename {dialogOpenState.node?.title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            To rename this node, enter its new name below.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Enter new name"
            type="text"
            fullWidth
            onChange={handleRenameInputChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleRenameOnClose} color="primary">
            Rename
          </Button>
        </DialogActions>
      </Dialog>

      <form noValidate autoComplete="off">
        <Grid container spacing={1} direction={"column"}>
          <Grid item xs={12}>
            <TextField
              id="outlined-basic"
              label="name"
              variant="outlined"
              onChange={handleNameInputChange}
              onSubmit={handleNodeInsert}
              fullWidth={true}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              fullWidth={true}
              size={"small"}
              onClick={handleNodeInsert}
            >
              insert node
            </Button>
          </Grid>
        </Grid>
      </form>

      <hr />

      <Dropdown overlay={treeState.menu} trigger={["contextMenu"]}>
        <Tree
          className="draggable-tree"
          defaultExpandedKeys={treeState.expandedKeys}
          draggable
          blockNode
          onDrop={onDrop}
          treeData={treeState.gData}
          allowDrop={allowDrop}
          onRightClick={handleRightClick}
          height={300} // La altura habilita virtual scrolling
        />
      </Dropdown>
    </>
  );
};

export default AntTreeComponent;
