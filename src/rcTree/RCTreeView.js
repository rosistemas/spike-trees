import React, { useContext, useEffect, useState } from 'react';
import classNames from 'classnames';
import Tree, { TreeNode } from 'rc-tree';
import './RCTreeStyles.css';

const RCTreeView = () => {
  let treeRef = React.createRef();

  const [keysState, setKeysState] = useState({
    defaultExpandedKeys: ['0-0-0-0'],
    defaultSelectedKeys: ['0-0-0-0'],
    defaultCheckedKeys: ['0-0-0-0'],
  });
  const onExpand = (expandedKeys) => {
    console.log('onExpand', expandedKeys);
  };

  const onSelect = (selectedKeys, info) => {
    console.log('selected', selectedKeys, info.node.props.eventKey);
  };

  const onCheck = (checkedKeys, info) => {
    console.log('onCheck', checkedKeys, info);
  };

  const onEdit = (selKey) => {
    setTimeout(() => {
      console.log('current key: ', selKey);
    }, 0);
  };

  const onDel = (e) => {
    if (!window.confirm('sure to delete?')) {
      return;
    }
    e.stopPropagation();
  };

  const setTreeRef = (tree) => {
    treeRef = tree;
  };

  const Icon = ({ selected }) => (
    <span
      className={classNames('customize-icon', selected && 'selected-icon')}
    />
  );

  const inflateData = () => {
    const treeData = [
      {
        key: '0',
        title: 'School',
        children: [
          {
            key: '0-0',
            title: 'Class A',
            children: [],
          },
          {
            key: '0-1',
            title: 'Class B',
            children: [],
          },
        ],
      },
    ];

    for (let index = 0; index < 100; index++) {
      treeData[0].children[0].children.push({
        key: `0-0-${index}`,
        title: 'Pupil class A',
      });
      treeData[0].children[1].children.push({
        key: `0-1-${index}`,
        title: 'Pupil class B',
      });
    }
    return treeData;
  };

  return (
    <>
      <div style={{ margin: '0 20px' }}>
        <h2>Check on Click TreeNode</h2>
        <Tree
          icon={Icon}
          className="myCls"
          showLine
          checkable={false}
          // selectable={false}
          selectable={true}
          defaultExpandAll
          onExpand={onExpand}
          defaultSelectedKeys={keysState.defaultSelectedKeys}
          defaultCheckedKeys={keysState.defaultCheckedKeys}
          onSelect={onSelect}
          onCheck={onCheck}
          treeData={inflateData()}
        />
      </div>
    </>
  );
};

export default RCTreeView;
